FROM maven:3-openjdk-11 AS builder
WORKDIR /build
COPY pom.xml .
RUN mvn dependency:go-offline

COPY src/ ./src/
RUN mvn package

FROM openjdk:11
WORKDIR /app
COPY --from=builder /build/target/rautenback-*.jar ./
CMD java -jar rautenback-*.jar --port 80 --properties /mnt/props/secret.properties
