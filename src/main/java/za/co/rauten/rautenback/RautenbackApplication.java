package za.co.rauten.rautenback;

import org.apache.commons.cli.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import za.co.rauten.rautenback.config.Config;

import java.io.IOException;
import java.util.Collections;

@SpringBootApplication
public class RautenbackApplication {

	public static void main(String[] args) {

		Options opts = new Options();
		opts.addOption(opt("port", "The port the server is to serve from."));
		opts.addOption(opt("properties", "The properties file containing necessary config."));

		try {
			CommandLine cmd = new DefaultParser().parse(opts, args);

			Config.initialise(cmd.getOptionValue("properties"));

			SpringApplication app = new SpringApplication(RautenbackApplication.class);
			app.setDefaultProperties(Collections.singletonMap("server.port", cmd.getOptionValue("port")));
			app.run(args);

		}catch (ParseException e){
			System.err.println("Unable to start rautenback server. Invalid command line args.");
			System.err.println(e.getMessage());
		}catch (IOException e){
			System.err.println("Unable to read properties file.");
		}
	}

	private static Option opt(String longName, String desc) {
		Option opt = new Option(null, longName, true, desc);
		opt.setRequired(true);
		opt.setOptionalArg(false);
		return opt;
	}

}
