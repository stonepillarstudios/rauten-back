package za.co.rauten.rautenback.db;

import za.co.rauten.rautenback.config.Config;
import za.co.rauten.rautenback.data.Project;
import za.co.rauten.rautenback.data.Technology;
import za.co.rauten.rautenback.data.VCSInfo;

import java.sql.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class DatabaseManager {

    private final Connection con;

    /**
     * Setup a new MySQL db connection, ready for transaction processing.
     * @throws Exception DB connection issue
     */
    public DatabaseManager() throws Exception{
        Config cfg = Config.getInstance();
        this.con = DriverManager.getConnection("jdbc:mysql://"+cfg.getDbHost()+":3306/"+cfg.getDbName(),
                cfg.getDbUser(),
                cfg.getDbPass());
    }

    public List<Project> getProjects() throws SQLException {
        LinkedList<Project> projects = new LinkedList<>();
        ResultSet res = con.createStatement().executeQuery(
                "SELECT projects.id, name, status, lastUpdated, description, image, " +
                        "website, playstore, loc, vcs, " +
                        "v.provider, v.user, v.repo, v.repogroup, v.public, " +
                        "t.slug, t.alt, t.link, t.img " +
                        "FROM projects LEFT JOIN vcs v on projects.vcs = v.id " +
                        "LEFT JOIN project_techs ON projects.id = project_techs.project_id LEFT JOIN techs t ON project_techs.tech_slug = t.slug " +
                        "ORDER BY projects.lastUpdated DESC, projects.id");
        // this insane query will get rows with all the relevant details, with project with multiple techs duplicated for
        // each tech.
        
        int lastId = -1;
        List<Technology> techList = null;
        Project tmpProject = null;
        
        while(res.next()){
        	
        	int id = res.getInt("projects.id");
        	if (id != lastId) {
        		if (lastId != -1) { // insert last project
        			assert tmpProject != null;
        			projects.add(tmpProject);
        		}
        		
        		// set up new project (this row)
        		techList = new LinkedList<>();
        		
        		Integer loc = res.getInt("loc");
                if (res.wasNull()) loc = null;
        		
        		tmpProject = new Project(
                        res.getInt("projects.id"), res.getString("name"), res.getString("status"),
                        res.getDate("lastUpdated"), res.getString("description"),
                        techList, res.getString("image"), res.getString("website"),
                        res.getString("playstore"), loc
                );
        		
        		// set vcs if avail
        		int vcsId = res.getInt("vcs");
                if (!res.wasNull()){
                    tmpProject.setVcs(new VCSInfo(vcsId, res.getString("v.provider"), res.getString("v.user"),
                            res.getString("v.repo"), res.getBoolean("v.repogroup"),
                            res.getBoolean("v.public")));
                }
                
        	}
        	
        	// add in tech details of this row (if any)
        	String techSlug = res.getString("t.slug");
        	if (!res.wasNull()) {
                assert techList != null;
                techList.add(new Technology(techSlug, res.getString("t.alt"),
						res.getString("t.link"), res.getString("t.img")));
        	}
        	
        	lastId = id;
        }
        // add in last row's project
        projects.add(tmpProject);
        res.close();
        
        return projects;
    }

    public List<VCSInfo> getVCSRecords() throws SQLException{
        List<VCSInfo> list = new LinkedList<>();
        ResultSet res = con.createStatement()
                .executeQuery("SELECT id, provider, user, repo, repogroup, public from vcs ORDER BY id DESC");
        while(res.next()){
            list.add(new VCSInfo(res.getInt("id"), res.getString("provider"), res.getString("user"),
                    res.getString("repo"), res.getBoolean("repogroup"), res.getBoolean("public")));
        }
        res.close();
        return list;
    }

    public List<Technology> getAllTechs() throws SQLException {
        List<Technology> techs = new LinkedList<>();
        Statement stmt = con.createStatement(); stmt.closeOnCompletion();
        ResultSet res = stmt.executeQuery("SELECT slug, alt, link, img FROM techs ORDER BY slug");
        while(res.next()){
            techs.add(new Technology(res.getString("slug"), res.getString("alt"),
                        res.getString("link"), res.getString("img")));
        }
        res.close(); // also closes stmt
        return techs;
    }

    public void updateProjectVisibility(int projectId, boolean isPublic) throws SQLException{
        PreparedStatement stmt = con.prepareStatement("UPDATE vcs SET public = ? WHERE id= " +
                "(SELECT vcs FROM projects WHERE id=?)");
        stmt.setBoolean(1, isPublic);
        stmt.setInt(2, projectId);
        stmt.executeUpdate();
        stmt.close();
    }

    public void updateProjectLastUpdated(int projectId, Date lastUpdated) throws SQLException{
        PreparedStatement stmt = con.prepareStatement("UPDATE projects SET lastUpdated=? WHERE id=?");
        stmt.setDate(1, new java.sql.Date(lastUpdated.getTime()));
        stmt.setInt(2, projectId);
        stmt.executeUpdate();
        stmt.close();
    }

    public void updateProjectLoc(int projectId, int loc) throws SQLException {
        PreparedStatement stmt = con.prepareStatement("UPDATE projects SET loc=? WHERE id=?");
        stmt.setInt(1, loc);
        stmt.setInt(2, projectId);
        stmt.executeUpdate();
        stmt.close();
    }

    public void createVCS(VCSInfo data) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(
                "INSERT INTO vcs (provider, user, repo, public, repogroup) VALUE (?, ?, ?, ?, ?)");
        stmt.setString(1, data.getProvider());
        stmt.setString(2, data.getUser());
        stmt.setString(3, data.getRepo());
        stmt.setBoolean(4, !data.isPrivate());
        stmt.setBoolean(5, data.isRepoGroup());
        stmt.executeUpdate();
        stmt.close();
    }

    public void updateVCS(int vcsID, VCSInfo data) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(
                "UPDATE vcs SET provider=?, user=?, repo=?, public=?, repogroup=? WHERE id=?");
        stmt.setString(1, data.getProvider());
        stmt.setString(2, data.getUser());
        stmt.setString(3, data.getRepo());
        stmt.setBoolean(4, !data.isPrivate());
        stmt.setBoolean(5, data.isRepoGroup());
        stmt.setInt(6, vcsID);
        stmt.executeUpdate();
        stmt.close();
    }

    public void createProject(Project data, int vcsID, String techSlugs) throws SQLException {
        PreparedStatement stmt = con.prepareStatement(
                "INSERT INTO projects " +
                        "(name, status, description, image, website, playstore, vcs) " +
                        "VALUE (?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        stmt.setString(1, data.getName());
        stmt.setString(2, data.getStatus());
        stmt.setString(3, data.getDescription());
        // nullable strings
        stmt.setString(4, data.getImage());
        stmt.setString(5, data.getWebsite());
        stmt.setString(6, data.getPlaystore());

        if (vcsID == -1)
            stmt.setNull(7, Types.INTEGER);
        else
            stmt.setInt(7, vcsID);

        stmt.executeUpdate();
        // get ID
        ResultSet keyRes = stmt.getGeneratedKeys();
        if (!keyRes.next())
        	throw new SQLException("Could not obtain new project's ID");
        
        int projId = keyRes.getInt(1);
        keyRes.close();
        stmt.close();
        
        // insert technologies
        techSlugs = techSlugs.replace(" ", "");
        stmt = con.prepareStatement("INSERT INTO project_techs (project_id, tech_slug) VALUES (?, ?)");
        for (String slug : techSlugs.split(",")) {
        	stmt.setInt(1, projId);
        	stmt.setString(2, slug);
        	stmt.executeUpdate();
        }
        stmt.close();
        
    }

    public void updateProject(int projID, Project data, int vcsID, String techSlugs) throws SQLException {
    	// update project
        PreparedStatement stmt = con.prepareStatement(
                "UPDATE projects SET name=?, status=?, description=?, image=?, website=?, " +
                        "playstore=?, vcs=? " +
                        "WHERE id=?");
        stmt.setString(1, data.getName());
        stmt.setString(2, data.getStatus());
        stmt.setString(3, data.getDescription());
        // nullable strings
        stmt.setString(4, data.getImage());
        stmt.setString(5, data.getWebsite());
        stmt.setString(6, data.getPlaystore());

        if (vcsID == -1)
            stmt.setNull(7, Types.INTEGER);
        else
            stmt.setInt(7, vcsID);

        stmt.setInt(8, projID);

        stmt.execute();
        stmt.close();
        
        // update techs
        techSlugs = techSlugs.replace(" ", "");
        // first, delete items not in comma-sep-list
        stmt = con.prepareStatement("DELETE FROM project_techs WHERE project_id = ? AND FIND_IN_SET(tech_slug, ?) = 0");
        stmt.setInt(1, projID);
        stmt.setString(2, techSlugs);
        stmt.execute();
        stmt.close();
        
        // then, add in items that need adding in
        stmt = con.prepareStatement("INSERT IGNORE INTO project_techs (project_id, tech_slug) "
        		+ "SELECT ?, slug FROM techs WHERE FIND_IN_SET(slug, ?) > 0");
        stmt.setInt(1, projID);
        stmt.setString(2, techSlugs);
        stmt.executeUpdate();
        stmt.close();
        
    }

    public void createTech(Technology tech) throws SQLException{
        PreparedStatement stmt = con.prepareStatement("INSERT INTO techs(slug, alt, link, img) VALUES (?, ?, ?, ?)");
        stmt.setString(1, tech.getSlug());
        stmt.setString(2, tech.getAlt());
        stmt.setString(3, tech.getLink());
        stmt.setString(4, tech.getImg());
        stmt.executeUpdate();
        stmt.close();
    }

    public void updateTech(Technology tech) throws SQLException{
        PreparedStatement stmt = con.prepareStatement("UPDATE techs SET alt=?, link=?, img=? WHERE slug=?");
        stmt.setString(1, tech.getAlt());
        stmt.setString(2, tech.getLink());
        stmt.setString(3, tech.getImg());
        stmt.setString(4, tech.getSlug());
        stmt.executeUpdate();
        stmt.close();
    }
    
    public void close() {
    	if (this.con != null)
    		try {
    			this.con.close();
    		}catch(SQLException e) {
    			System.err.println("Error closing DB: " + e.getMessage());
    		}
    }

}
