package za.co.rauten.rautenback.extern;

import org.json.JSONObject;
import za.co.rauten.rautenback.data.VCSInfo;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Random;

/**
 * Internal class for wrapping the LoCulator API.
 * Note that the service does not work for repogroups.
 */
public class LoculatorAPI {
    private final VCSInfo vcsInfo;
    private final String commitHash;
    private final String repoOverride;

    /**
     * Init the object.
     * @param vcsInfo
     * @param commitHash
     * @param repoOverride Optional. If set, will use this repo instead of the one in vcsInfo. Useful for repoGroups
     */
    public LoculatorAPI(VCSInfo vcsInfo, String commitHash, String repoOverride) {
        this.vcsInfo = vcsInfo;
        this.commitHash = commitHash;
        this.repoOverride = repoOverride;
    }

    public LoculatorAPI(VCSInfo vcsInfo, String commitHash){
        this(vcsInfo, commitHash, null);
    }

    /**
     * Long-running method that calls on the external LoCulator service.
     * Uses exponential backoff with checking if line-counting is done.
     * @return The lines of code in the project (including blanks), or null if something went wrong.
     */
    public Integer getLOC(){

        try {

            Response resp = ClientBuilder.newClient().target(buildLoculatorURL())
                    .request(MediaType.APPLICATION_JSON).get();

            int collisions = 0;
            while (resp.getStatus() == 202) {  // busy running that side
            	resp.close();
                collisions++;
                if (collisions >= 10) // give up
                    return null;

                // exponential backoff sleep
                int sleepTime = (int) Math.pow(2, collisions) * 1000;
                Thread.sleep(sleepTime);

                resp = ClientBuilder.newClient().target(buildLoculatorURL())
                        .request(MediaType.APPLICATION_JSON).get();
            }

            if (resp.getStatus() == 200) {
                JSONObject respData = new JSONObject(resp.readEntity(String.class));
                Object resO = respData.get("results");
                if (!(resO instanceof JSONObject)) return null; // remote cache issue

                JSONObject res = ((JSONObject) resO);
                if (res.has("error")) return null;

                return res.getInt("total_lines"); // including blanks for now

            } else // unexpected response
                return null;

        }catch (Exception e){
            // network issue, or interrupt
            System.err.println("Error during loculation: " + e.getMessage());
            return null;
        }
    }

    private String buildLoculatorURL(){
        String url = String.format("https://loculator.singularity.net.za/api/loc/%s/%s/%s?latest_commit=%s",
                vcsInfo.getProviderDomain(), vcsInfo.getUser(), repoOverride == null ? vcsInfo.getRepo() : repoOverride
                , commitHash);
        if (vcsInfo.isPrivate())
            url += "&private=true";
        return url;
    }
}
