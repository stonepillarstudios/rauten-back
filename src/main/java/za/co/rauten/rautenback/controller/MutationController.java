package za.co.rauten.rautenback.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.rauten.rautenback.data.Project;
import za.co.rauten.rautenback.data.Technology;
import za.co.rauten.rautenback.data.VCSInfo;
import za.co.rauten.rautenback.db.DatabaseManager;

@CrossOrigin(origins = "*")
@RestController
public class MutationController {

    @PostMapping(value="/project", consumes="application/json")
    public ResponseEntity<Void> createProject(@RequestBody Project proj, @RequestParam String pwd) throws Exception {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        DatabaseManager db = new DatabaseManager();
        db.createProject(proj, proj.getVcsID(), proj.getTechSlugs());
        db.close();
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping(value="/vcs", consumes="application/json")
    public ResponseEntity<Void> createVCS(@RequestBody VCSInfo data, @RequestParam String pwd) throws Exception {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        DatabaseManager db = new DatabaseManager();
        db.createVCS(data);
        db.close();
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping(value="/tech", consumes="application/json")
    public ResponseEntity<Void> createTech(@RequestBody Technology data, @RequestParam String pwd) throws Exception {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        DatabaseManager db = new DatabaseManager();
        db.createTech(data);
        db.close();
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(value="/tech", consumes="application/json")
    public ResponseEntity<Void> updateTech(@RequestBody Technology data, @RequestParam String pwd) throws Exception {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        DatabaseManager db = new DatabaseManager();
        db.updateTech(data);
        db.close();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value="/project/{projID}", consumes="application/json")
    public ResponseEntity<Void> updateProject(
            @PathVariable int projID, @RequestBody Project proj, @RequestParam String pwd) throws Exception {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        DatabaseManager db = new DatabaseManager();
        db.updateProject(projID, proj, proj.getVcsID(), proj.getTechSlugs());
        db.close();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(value="/vcs/{vcsID}", consumes="application/json")
    public ResponseEntity<Void> updateVCS(
            @PathVariable int vcsID, @RequestBody VCSInfo data, @RequestParam String pwd) throws Exception {
        if (!AuthController.internalCheckPassword(pwd))
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);

        DatabaseManager db = new DatabaseManager();
		db.updateVCS(vcsID, data);
		db.close();
        return ResponseEntity.ok().build();
    }

}
