package za.co.rauten.rautenback.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import za.co.rauten.rautenback.config.Config;

/**
 * Keeping it simple
 */
@CrossOrigin(origins = "*")
@RestController
public class AuthController {

    @GetMapping("/checkpwd")
    public ResponseEntity checkPassword(@RequestParam String pwd){
        if (internalCheckPassword(pwd))
            return ResponseEntity.ok().build();
        else
            return new ResponseEntity(HttpStatus.FORBIDDEN);
    }

    static boolean internalCheckPassword(String pwd){
        return Config.getInstance().getDashKey().equals(pwd);
    }

}
