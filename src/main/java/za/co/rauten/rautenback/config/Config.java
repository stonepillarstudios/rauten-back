package za.co.rauten.rautenback.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    private static Config config = null;
    private final Properties props;

    private Config(String propFile) throws IOException {
        props = new Properties();
        props.load(new FileInputStream(propFile));
    }

    public static void initialise(String propertiesFile) throws IOException {
        if (config == null)
            config = new Config(propertiesFile);
    }

    public static Config getInstance(){
        if (config == null)
            throw new IllegalStateException("Config has not been initialised with a properties file.");
        return config;
    }

    public String getDbName(){
        return props.getProperty("db_name");
    }

    public String getDbUser(){
        return props.getProperty("db_user");
    }

    public String getDbPass(){
        return props.getProperty("db_pass");
    }

    public String getDbHost(){
        return props.getProperty("db_host");
    }

    public String getBitbucketKey(){
        return props.getProperty("bitbucket_key");
    }

    public String getBitbucketUser(){
        return props.getProperty("bitbucket_usr");
    }

    public String getGithubKey(){
        return props.getProperty("github_key");
    }

    public String getGithubUser(){
        return props.getProperty("github_usr");
    }

    public String getGitlabKey(){
        return props.getProperty("gitlab_key");
    }

    public String getGitlabUser(){
        return props.getProperty("gitlab_usr");
    }

    public String getDashKey(){
        return props.getProperty("dash_key");
    }
}
